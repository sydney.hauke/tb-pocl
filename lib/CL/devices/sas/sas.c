/*
 * Author : Sydney Hauke
 * Created : 06.08.18
 * File : sas.h
 * 
 * Description : POCL device driver for the SAS (Separate address space) device
 */

#include "config.h"
#include "cpuinfo.h"
#include "basic/basic.h"
#include "topology/pocl_topology.h"
#include "common.h"
#include "utlist.h"
#include "devices.h"
#include "pocl_util.h"

#include <assert.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <utlist.h>

#include "pocl_cache.h"
#include "pocl_timing.h"
#include "pocl_file_util.h"
#include "bufalloc.h"

#include <pocl_cl.h>
#include <pocl.h>

#ifdef OCS_AVAILABLE
#include "pocl_llvm.h"
#endif

#include "sas.h"

static ssize_t
write_pipe(int pipe_fd, const void *buf, size_t len)
{
  ssize_t nb_written;
  size_t to_write = len;
  const void *buf_p = buf;

  while(1) {
    if(to_write == 0) break;

    nb_written = write(pipe_fd, buf_p, to_write);

    if(nb_written < 0) {
      return nb_written;
    }

    to_write -= nb_written;
    buf_p = (char*)buf_p + nb_written;
  }

  return len;
}

static ssize_t
read_pipe(int pipe_fd, void *buf, size_t len)
{
  ssize_t nb_read;
  size_t to_read = len;
  void *buf_p = buf;

  while(1) {
    if(to_read == 0) break;

    nb_read = read(pipe_fd, buf_p, to_read);

    if(nb_read < 0) {
      return nb_read;
    }

    to_read -= nb_read;
    buf_p = (char*)buf_p + nb_read;
  }

  return len;
}

struct data {
  /* Currently loaded kernel. */
  cl_kernel current_kernel;
  /* Loaded kernel dynamic library handle. */
  lt_dlhandle current_dlhandle;
  
  /* List of commands ready to be executed */
  _cl_command_node * volatile ready_list;
  /* List of commands not yet ready to be executed */
  _cl_command_node * volatile command_list;
  /* Lock for command list related operations */
  pocl_lock_t cq_lock;
  /* printf buffer */
  void *printf_buffer;

  /* VERY IMPORTANT. The struct layout is the same as basic's
  * "struct data" until HERE. Because we reuse basic's code, their
  * code references basic's "struct data", thus their struct layout 
  * and not ours. If we modify the layout before here, their code
  * won't reference the correct objects inside our struct because
  * their code reference their "struct data".
  * 
  * VERY BAD CODE INDEED.
  */

  /* Pipes used as communication channels */
  int data_rd_pipe, data_wr_pipe;
  int cmd_pipe, rsp_pipe;

  /* Bufalloc allocator objects. They keep track of memory allocations in the 
   * device's memory regions. */
  memory_region_t global_mem;
  memory_region_t local_mem;
};

void
pocl_sas_init_device_ops(struct pocl_device_ops *ops)
{
    pocl_basic_init_device_ops(ops);

    ops->device_name = SAS_NAME;

    /* TODO : reassign functions specific to SAS */
  
    ops->init = pocl_sas_init;
    ops->uninit = pocl_sas_uninit;
    ops->reinit = pocl_sas_reinit;
    ops->probe = pocl_sas_probe;
    ops->read = pocl_sas_read;
    ops->write = pocl_sas_write;
    ops->run = pocl_sas_run;
    ops->build_hash = pocl_sas_build_hash;
    ops->alloc_mem_obj = pocl_sas_alloc_mem_obj;
    ops->free = pocl_sas_free;
    ops->free_ptr = pocl_sas_free_ptr;

}

cl_int
pocl_sas_init(unsigned j, cl_device_id device, const char *parameters)
{
  struct data *d;
  void *global_mem_base;
  ssize_t written, read;
  unsigned int status;
  cl_int ret = CL_SUCCESS;
  int err;
  static int first_sas_init = 1;

  if (first_sas_init)
    {
      POCL_MSG_WARN ("INIT dlcache DOTO delete\n");
      pocl_init_dlhandle_cache();
      first_sas_init = 0;
    }
  device->global_mem_id = 0;

  d = (struct data *) calloc (1, sizeof (struct data));
  if (d == NULL)
    return CL_OUT_OF_HOST_MEMORY;

  d->current_kernel = NULL;
  d->current_dlhandle = 0;
  device->data = d;

  pocl_init_cpu_device_infos (device);

  /* hwloc probes OpenCL device info at its initialization in case
     the OpenCL extension is enabled. This causes to printout 
     an unimplemented property error because hwloc is used to
     initialize global_mem_size which it is not yet. Just put 
     a nonzero there for now. */
  device->global_mem_size = 1;
  err = pocl_topology_detect_device_info(device);
  if (err)
    ret = CL_INVALID_DEVICE;

  POCL_INIT_LOCK (d->cq_lock);

  assert (device->printf_buffer_size > 0);
  d->printf_buffer = pocl_aligned_malloc (MAX_EXTENDED_ALIGNMENT,
                                          device->printf_buffer_size);
  assert (d->printf_buffer != NULL);

  pocl_cpuinfo_detect_device_info(device);
  pocl_set_buffer_image_limits(device);

  /* in case hwloc doesn't provide a PCI ID, let's generate
     a vendor id that hopefully is unique across vendors. */
  const char *magic = "pocl";
  if (device->vendor_id == 0)
    device->vendor_id =
      magic[0] | magic[1] << 8 | magic[2] << 16 | magic[3] << 24;

  device->vendor_id += j;

  /* The basic driver represents only one "compute unit" as
     it doesn't exploit multiple hardware threads. Multiple
     basic devices can be still used for task level parallelism 
     using multiple OpenCL devices. */
  device->max_compute_units = 1;


  /* Open pipes in this particular order */
  d->cmd_pipe = open(CMD_PIPE_PATHNAME, O_WRONLY);
  if(d->cmd_pipe < 0) {
    ret = CL_DEVICE_NOT_AVAILABLE;
    goto _out;
  }
  d->rsp_pipe = open(RSP_PIPE_PATHNAME, O_RDONLY);
  if(d->rsp_pipe < 0) {
    ret = CL_DEVICE_NOT_AVAILABLE;
    goto _out;
  }
  d->data_wr_pipe = open(DATA_WR_PIPE_PATHNAME, O_WRONLY);
  if(d->data_wr_pipe < 0) {
    ret = CL_DEVICE_NOT_AVAILABLE;
    goto _out;
  }
  d->data_rd_pipe = open(DATA_RD_PIPE_PATHNAME, O_RDONLY);
  if(d->data_rd_pipe < 0) {
    ret = CL_DEVICE_NOT_AVAILABLE;
    goto _out;
  }

  /* Everything went well, we can now talk to the device */
  SEND_CMD(d->cmd_pipe, CMD_GET_GLOBAL_BASE_ADDR, 0, 0, &written);
  read = read_pipe(d->data_rd_pipe, &global_mem_base, sizeof(global_mem_base));
  RCV_RESP(d->rsp_pipe, &status);
  // TODO check status

  init_mem_region(&d->global_mem, (memory_address_t)global_mem_base, GLOBAL_MEM_SIZE);

_out:
  return ret;
}

cl_int
pocl_sas_uninit(cl_device_id device)
{
  /* TODO : free resources */
  return CL_SUCCESS;
}

cl_int
pocl_sas_reinit(cl_device_id device)
{
  /* TODO : reinit resources */
  return CL_SUCCESS;
}

unsigned int
pocl_sas_probe(struct pocl_device_ops *ops)
{
    int ret = 0;

    /* Check pipes presence */
    ret |= access(CMD_PIPE_PATHNAME, F_OK | W_OK);
    ret |= access(RSP_PIPE_PATHNAME, F_OK | R_OK);
    ret |= access(DATA_RD_PIPE_PATHNAME, F_OK | R_OK);
    ret |= access(DATA_WR_PIPE_PATHNAME, F_OK | W_OK);

    /* No error, one device found */
    return !ret;
}

cl_int
pocl_sas_alloc_mem_obj(cl_device_id device, cl_mem mem_obj, void *host_ptr)
{
  // TODO
  cl_mem_flags flags = mem_obj->flags;
  struct data *d = (struct data*)device->data;
  unsigned i;

  POCL_MSG_PRINT_MEMORY (" mem %p, dev %d\n", mem_obj, device->dev_id);

  /* check if some driver has already allocated memory for this mem_obj 
     in our global address space, and use that*/
  for (i = 0; i < mem_obj->context->num_devices; ++i)
  {
    if (!mem_obj->device_ptrs[i].available)
      continue;

    if (mem_obj->device_ptrs[i].global_mem_id == device->global_mem_id
        && mem_obj->device_ptrs[i].mem_ptr != NULL)
    {
      mem_obj->device_ptrs[device->dev_id].mem_ptr =
        mem_obj->device_ptrs[i].mem_ptr;

      return CL_SUCCESS;
    }
  }

  chunk_info_t *chunk = alloc_buffer(&d->global_mem, mem_obj->size);
  if(chunk == NULL) {
    return CL_MEM_OBJECT_ALLOCATION_FAILURE;
  }

  mem_obj->device_ptrs[device->dev_id].mem_ptr = (void*)chunk;

  return CL_SUCCESS;
}

void
pocl_sas_free_ptr(cl_device_id device, void *mem_ptr)
{
  // TODO
}

void
pocl_sas_free(cl_device_id device, cl_mem memobj)
{
  chunk_info_t *chunk = 
          (chunk_info_t*)memobj->device_ptrs[device->dev_id].mem_ptr;

  free_chunk(chunk);
}

void
pocl_sas_read(void *data,
              void *__restrict__ dst_host_ptr,
              pocl_mem_identifier *src_mem_id,
              cl_mem src_buf,
              size_t offset,
              size_t size)
{
  struct data *d = (struct data*)data;
  void *__restrict__ device_ptr = src_mem_id->mem_ptr;
  chunk_info_t *chunk = (chunk_info_t*)device_ptr;
  ssize_t nb_written;
  ssize_t nb_read;
  uint8_t status;

  POCL_MSG_PRINT_GENERAL("Reading %lu bytes at address %p from device\n", 
                         size, chunk->start_address + offset);

  /* Send read command to device */
  SEND_CMD(d->cmd_pipe, 
           CMD_RD_BUF, 
           chunk->start_address + offset,
           size,
           &nb_written);
  // TODO : check nb_written
  nb_written = read_pipe(d->data_rd_pipe, dst_host_ptr, size);
  RCV_RESP(d->rsp_pipe, &status);
  // TODO : check status
}

void
pocl_sas_write(void *data,
               const void *__restrict__ src_host_ptr,
               pocl_mem_identifier *dst_mem_id,
               cl_mem dst_buf,
               size_t offset,
               size_t size)
{
  struct data *d = (struct data*)data;
  void *__restrict__ device_ptr = dst_mem_id->mem_ptr;
  chunk_info_t *chunk = (chunk_info_t*)device_ptr;
  unsigned int rsp_packet[RSP_PACKET_ELEMENTS];
  size_t to_write = size;
  ssize_t written;
  ssize_t nb_read;

  POCL_MSG_PRINT_GENERAL("Writing %lu bytes at address %p of device\n", 
                          size, chunk->start_address + offset);

  /* Send write command to device */
  SEND_CMD(d->cmd_pipe, 
           CMD_WR_BUF, 
           chunk->start_address + offset,
           size, 
           &written);
  if(written < CMD_PACKET_SIZE) {
    POCL_MSG_ERR("command write to device failed");
    return;
  }

  /* Send data to device buffer */
  written = write_pipe(d->data_wr_pipe, src_host_ptr, size);
  if(written < 0) {
    POCL_MSG_ERR("Writing to device failed");
  }

  /* Wait for response */
  nb_read = read(d->rsp_pipe, &rsp_packet, RSP_PACKET_FIELD_SIZE);
  if(nb_read < RSP_PACKET_FIELD_SIZE) {
    POCL_MSG_ERR("response read failed");
    return;
  }

  // TODO : check response and do something about it
}

void
pocl_sas_run(void *data, _cl_command_node *cmd)
{
  struct data *d = (struct data*)data;
  struct pocl_context *pc = &cmd->command.run.pc;

  char *binary_path = NULL;
  char *workgroup_name = NULL;
  size_t pathname_len, wg_name_len;

  cl_kernel kernel = cmd->command.run.kernel;
  cl_program program = kernel->program;
  cl_device_id device = cmd->device;
  int dev_index = pocl_cl_device_to_index(program, device);

  size_t x, y, z;
  size_t i;
  struct pocl_argument *pocl_arg;
  void **arguments;
  void *global_mem_base;

  ssize_t nb_written;
  uint32_t status;
  int ret;

  binary_path = malloc(POCL_FILENAME_LENGTH);
  workgroup_name = malloc(WORKGROUP_NAME_LENGTH);
  arguments = malloc((kernel->num_args + kernel->num_locals) * sizeof(void*));

  // TODO : what to do if malloc failed ?
  assert(binary_path != NULL);
  assert(workgroup_name != NULL);
  assert(arguments != NULL);

  pocl_cache_final_binary_path(binary_path, program, dev_index, kernel,
                               cmd->command.run.local_x,
                               cmd->command.run.local_y,
                               cmd->command.run.local_z);
  if(!pocl_exists(binary_path)) {
    pocl_cache_final_binary_path(binary_path, program, dev_index, kernel, 0,0,0);
    if(!pocl_exists(binary_path)) {
      POCL_ABORT("binary %s does not exit", binary_path);
    }
  }

  ret = snprintf(workgroup_name, 
                  WORKGROUP_NAME_LENGTH, 
                  "_pocl_launcher_%s_workgroup",
                  kernel->name);
  if(ret < 0) {
    // TODO
  }

  // Length includes null terminator
  pathname_len = strlen(binary_path) + 1;
  wg_name_len = strlen(workgroup_name) + 1;

  /* Process all kernel arguments */
  for(i = 0; i < kernel->num_args; i++) {
    pocl_arg = &(cmd->command.run.arguments[i]);

    if(kernel->arg_info[i].is_local) {
      // TODO
      POCL_ABORT_UNIMPLEMENTED("local arguments");
    }
    else if(kernel->arg_info[i].type == POCL_ARG_TYPE_POINTER) {
      // TODO : check global/local/constant base addresses on the device
      if (pocl_arg->value == NULL) {
        arguments[i] = NULL;
      }
      else
        arguments[i] = ((chunk_info_t*)((*(cl_mem *) (pocl_arg->value))->device_ptrs[cmd->device->dev_id].mem_ptr))->start_address;
    }
    else if(kernel->arg_info[i].type == POCL_ARG_TYPE_SAMPLER) {
      // TODO
      POCL_ABORT_UNIMPLEMENTED("sampler arguments");
    }
    else {
      arguments[i] = (void*)(pocl_arg->value);
    }
  }

  /* Process all locals */
  for(i = kernel->num_args; i < kernel->num_locals + kernel->num_args; i++) {
    /* TODO : memory alignment ? */
    arguments[i] = (cmd->command.run.arguments[i]).value;
  }

  /* Setup POCL context */
  pc->local_size[0] = cmd->command.run.local_x;
  pc->local_size[1] = cmd->command.run.local_y;
  pc->local_size[2] = cmd->command.run.local_z;

  /* PRINT BUFFERS UNSUPPORTED */

  // Send workgroup function to device
  SEND_CMD(d->cmd_pipe, CMD_SET_DL_OBJ_PATH, pathname_len, wg_name_len, &nb_written);
  // TODO : check error while writing pipe


  // Send pathname INCLUDING the null terminator
  nb_written = write_pipe(d->data_wr_pipe, binary_path, pathname_len);

  // Send workgroup name INCLUDING the null terminator
  nb_written = write_pipe(d->data_wr_pipe, workgroup_name, wg_name_len);
  RCV_RESP(d->rsp_pipe, &status);
  // TODO : check response code

  unsigned int nb_arguments = kernel->num_args + kernel->num_locals;

  SEND_CMD(d->cmd_pipe, 
           CMD_SET_ARGS, 
           nb_arguments, 
           0, 
           &nb_written);
  // TODO : test written bytes

  nb_written = write_pipe(d->data_wr_pipe, arguments, sizeof(void*) * nb_arguments);
  RCV_RESP(d->rsp_pipe, &status);

  for (z = 0; z < pc->num_groups[2]; z++) {
    for(y = 0; y < pc->num_groups[1]; y++) {
      for(x = 0; x < pc->num_groups[0]; x++) {
        pc->group_id[0] = x;
        pc->group_id[1] = y;
        pc->group_id[2] = z;

        /* Send POCL context */
        SEND_CMD(d->cmd_pipe, CMD_SET_CONTEXT, sizeof(struct pocl_context), 0, &nb_written);
        nb_written = write_pipe(d->data_wr_pipe, pc, sizeof(struct pocl_context));
        RCV_RESP(d->rsp_pipe, &status);

        /* Start computation and wait done */
        SEND_CMD(d->cmd_pipe, CMD_START_COMP, 0, 0, &nb_written);
        RCV_RESP(d->rsp_pipe, &status); 
      }
    }
  }

  POCL_MEM_FREE(binary_path);
  POCL_MEM_FREE(workgroup_name);
  POCL_MEM_FREE(arguments);
}

char *
pocl_sas_build_hash(cl_device_id device)
{
  char* res = calloc(1000, sizeof(char));
#ifdef KERNELLIB_HOST_DISTRO_VARIANTS
  char *name = get_llvm_cpu_name ();
  snprintf (res, 1000, "%s-%s-%s", SAS_NAME, HOST_DEVICE_BUILD_HASH, name);
  POCL_MEM_FREE (name);
#else
  snprintf (res, 1000, "%s-%s", SAS_NAME, HOST_DEVICE_BUILD_HASH);
#endif
  return res;

}