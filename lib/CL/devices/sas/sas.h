/*
 * Author : Sydney Hauke
 * Created : 06.08.18
 * File : sas.h
 * 
 * Description : header file for the SAS device
 */

#ifndef SAS_H
#define SAS_H

#include "pocl_cl.h"
#include "pocl_icd.h"
#include "config.h"
#include "prototypes.inc"

#define SAS_NAME    "sas"

#define DEFAULT_WG_SIZE   4096
#define GLOBAL_MEM_SIZE   (1 << 25) // 32 MB

#define DATA_WR_PIPE_PATHNAME   "/tmp/sas_data_wr"
#define DATA_RD_PIPE_PATHNAME   "/tmp/sas_data_rd"
#define CMD_PIPE_PATHNAME       "/tmp/sas_cmd"
#define RSP_PIPE_PATHNAME       "/tmp/sas_rsp"

#define CMD_RD_BUF                  0x0
#define CMD_WR_BUF                  0x1
#define CMD_SET_DL_OBJ_PATH         0x2
#define CMD_SET_ARGS                0x3
#define CMD_SET_CONTEXT             0x4
#define CMD_START_COMP              0x5
#define CMD_GET_GLOBAL_BASE_ADDR    0x6

#define CMD_PACKET_SIZE             17 // In bytes

#define RSP_DONE                    0x0
#define RSP_FAIL                    0x1

#define RSP_PACKET_FIELD_SIZE       1   // 8 bit fields
#define RSP_PACKET_ELEMENTS         1

#define WORKGROUP_NAME_LENGTH       512

#define SEND_CMD(cmd_pipe, cmd, arg0, arg1, written)            \
    do {                                                        \
        uint8_t cmd_packet[CMD_PACKET_SIZE];                    \
        *(uint8_t*)cmd_packet = cmd;                            \
        *(uint64_t*)(cmd_packet + 1) = (uint64_t)arg0;          \
        *(uint64_t*)(cmd_packet + 1 + sizeof(uint64_t)) =       \
                                           (uint64_t)arg1;      \
                                                                \
        *written = write_pipe(cmd_pipe,                         \
                   cmd_packet,                                  \
                   CMD_PACKET_SIZE);                            \
                                                                \
    } while(0);                                                 \

#define RCV_RESP(rsp_pipe, status)                              \
    do {                                                        \
        ssize_t read = read_pipe(rsp_pipe,                      \
                                 status,                        \
                                 RSP_PACKET_FIELD_SIZE);        \
        /* TODO */                                              \
    } while(0);                                                 \

GEN_PROTOTYPES(sas)

#endif /* SAS_H */